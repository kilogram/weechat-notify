
TARGET = libweechatnotify.so
SRCDIR = src
BUILDDIR = build
SRCS = $(wildcard $(SRCDIR)/*.c)
OBJS = $(SRCS:$(SRCDIR)/%.c=$(BUILDDIR)/%.o)
INCS = $(SRCS:$(SRCDIR)/%.c=$(BUILDDIR)/%.d)
LIBRARIES = libnotify glib-2.0 gdk-3.0

CFLAGS = -fPIC -Wall -Wextra -O2 -g
CFLAGS += $(shell pkg-config --cflags $(LIBRARIES))

LDFLAGS = -shared
LDFLAGS += -Wl,-rpath,/usr/include

LIBS = $(shell pkg-config --libs $(LIBRARIES))

all: $(BUILDDIR)/$(TARGET)

.PHONY: $(BUILDDIR)
$(BUILDDIR):
	@-mkdir -p $(BUILDDIR)

$(OBJS):$(BUILDDIR)/%.o:$(SRCDIR)/%.c
	@echo "[CC] $@"
	@$(CC) $(CFLAGS) -c -o $@ $<

$(BUILDDIR)/$(TARGET): $(OBJS)
	@echo "[LD] $@"
	@$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

$(INCS):$(BUILDDIR)/%.d:$(SRCDIR)/%.c | $(BUILDDIR)
	@$(CC) $(CFLAGS) -MM $< >$@

include $(INCS)

.PHONY: clean
clean:
	-rm -rf $(BUILDDIR)
