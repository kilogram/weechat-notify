// message.c: notification management
// Copyright (C) 2016  Kevin George

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.   See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.   If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <weechat/weechat-plugin.h>

#include "message.h"


#define UNUSED __attribute__((unused))


WEECHAT_PLUGIN_NAME("notify");
WEECHAT_PLUGIN_DESCRIPTION("Notifications with libnotify");
WEECHAT_PLUGIN_AUTHOR("Kevin George <kevingeorgetm@gmail.com>");
WEECHAT_PLUGIN_VERSION("0.1a1");
WEECHAT_PLUGIN_LICENSE("GPL3");
WEECHAT_PLUGIN_PRIORITY(1000);


int notify_msg_print_cb(
        const void *pointer,
        void *data,
        struct t_gui_buffer *buffer,
        time_t date,
        int tags_count,
        const char **tags,
        int displayed,
        int highlight,
        const char *prefix,
        const char *message)
{
    return WEECHAT_RC_OK;
}
