// message.h: notification management
// Copyright (C) 2016  Kevin George

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.   See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.   If not, see <http://www.gnu.org/licenses/>.


#ifndef _WEECHAT_NOTIFY_MESSAGE_H
#define _WEECHAT_NOTIFY_MESSAGE_H


#include <weechat/weechat-plugin.h>


int notify_msg_print_cb(
        const void *pointer,
        void *data,
        struct t_gui_buffer *buffer,
        time_t date,
        int tags_count,
        const char **tags,
        int displayed,
        int highlight,
        const char *prefix,
        const char *message);


#endif // _WEECHAT_NOTIFY_MESSAGE_H
