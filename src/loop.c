// notify.c: notification management
// Copyright (C) 2016  Kevin George

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.   See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.   If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <semaphore.h>
#include <gdk/gdk.h>

#include "loop.h"


#define UNUSED __attribute__((unused))


static pthread_t notifier_thread;
static sem_t notifier_thread_ready;
static int notifier_thread_status;
static GMainLoop *loop;


void *notifier(void *unused UNUSED)
{
    loop = g_main_loop_new(NULL, FALSE);
    if (loop == NULL) {
        goto notifier_err;
    } else {
        notifier_thread_status = 1;
        sem_post(&notifier_thread_ready);
    }

    g_main_loop_run(loop);

    return NULL;

notifier_err:
    notifier_thread_status = 0;
    sem_post(&notifier_thread_ready);
    return NULL;
}

bool notify_start(void)
{
    if (sem_init(&notifier_thread_ready, 0, 0) != 0) {
        goto notify_start_err0;
    }

    if (pthread_create(&notifier_thread, NULL, notifier, NULL) != 0) {
        goto notify_start_err0;
    }

    sem_wait(&notifier_thread_ready);
    if (notifier_thread_status == 0) {
        goto notify_start_err0;
    }

    return true;

notify_start_err0:
    return false;
}

void notify_stop(void)
{
    g_main_loop_quit(loop);
    pthread_join(notifier_thread, NULL);
}
