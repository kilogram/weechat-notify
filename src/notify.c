// notify.c: entry point for library
// Copyright (C) 2016  Kevin George

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.   See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.   If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>

#include <dlfcn.h>
#include <pthread.h>
#include <semaphore.h>
#include <gdk/gdk.h>
#include <weechat/weechat-plugin.h>

#include "notify.h"
#include "loop.h"
#include "message.h"


#define UNUSED __attribute__((unused))


struct t_weechat_plugin *weechat_plugin;

int weechat_plugin_init(struct t_weechat_plugin *plugin,
                        int argc UNUSED,
                        char* argv[] UNUSED)
{
    weechat_plugin = plugin;

    if (!notify_start()) {
        return WEECHAT_RC_ERROR;
    }

    weechat_hook_print(NULL, NULL, NULL, 1, notify_msg_print_cb, NULL, NULL);

    return WEECHAT_RC_OK;
}

int weechat_plugin_end(struct t_weechat_plugin *plugin UNUSED)
{
    notify_stop();
    return WEECHAT_RC_OK;
}
